﻿const express = require('express');
const router = express.Router();
const sensorService = require('./sensor.service');

// routes
router.get('/data', getSensorData);

module.exports = router;

function getSensorData(req, res, next) {
    sensorService.getData()
        .then(data => data ? res.json(data) : res.sendStatus(404))
        .catch(err => next(err));
}