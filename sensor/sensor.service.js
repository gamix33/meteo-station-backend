﻿module.exports = { getData };

const fs = require('fs');

function getNewestDataFromCsv(data) {
  const dataRows = data.split('\n');
  const newestRow = dataRows[dataRows.length-1];
  const newestDataValues = newestRow.split(',');

  //labels of sensor data items saved in single row of csv file, order is important
  const labels = ['humidity', 'temperature', 'light', 'rain', 'pressure'];

  const result = {};
  for (let i = 0; i < labels.length; i++) {
    result[labels[i]] = newestDataValues[i];
  }
  return result;
}

async function getData() {
    try {
        const csvContent = fs.readFileSync('C://sensor_data.csv', 'utf8');
        console.log(csvContent);
        return getNewestDataFromCsv(csvContent);
    } catch(err) {
        console.log('Error:', e.stack);
        throw err;
    }
}