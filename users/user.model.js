const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    notification: { type: String, required: true }, //NONE|SMS|EMAIL
    enabledNotifications: { type: Array, required: true }, //[TEMPERATURE|LIGHT|HUMIDITY|RAIN|PRESSURE]
    weatherWidget: { type: String, required: true }, //INTERIA|ONET
    diseases: { type: Array, required: true },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);