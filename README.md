# meteo-station-backend

1. Install [NodeJS](https://nodejs.org/en/download/) and [NPM](https://www.npmjs.com/get-npm)
2. Install [MongoDB Server](https://www.mongodb.com/download-center/community)
3. Run `MongoDB` (on Windows - create directory `/data/db` and run from the command line `mongod.exe`)
4. Run `npm install` from the command line in the project root folder.
5. Start API by running `npm start`, you should see the message: `Server listening on port 4000`
6. Now you can run `meteo-station-frontend`.

-----------

For explore data stored in `MongoDB` you can use [MongoDB Compass](https://www.mongodb.com/download-center/compass).

-----------

Arduino sensor data must be placed in `csv` file, available on `C:\sensor_data.csv`.

Sample format (`sensor_data.csv`):

`humidity,temperature[C],light[lux],rain[NO_RAIN|SMALL_RAIN|MEDIUM_RAIN|STRONG_RAIN],pressure[hPa]`  
`36,15,492,NO_RAIN,990`  
`33,21,546,SMALL_RAIN,1050`  
`45,9,522,MEDIUM_RAIN,910`  
`30,19,605,STRONG_RAIN,1005`  
`37,24,596,NO_RAIN,986`  

Header is not required.  
Newest data should be placed at the end of the file.